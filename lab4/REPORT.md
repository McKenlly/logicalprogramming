## Отчет по лабораторной работе №4
## по курсу "Логическое программирование"

## Обработка естественного языка

### студент: Бокоч С.М.

## Результат проверки

| Преподаватель     | Дата         |  Оценка       |
|-------------------|--------------|---------------|
| Сошников Д.В. |              |               |
| Левинская М.А.|              |               |

> *Комментарии проверяющих (обратите внимание, что более подробные комментарии возможны непосредственно в репозитории по тексту программы)*


## Введение
Задача понимания естественного языка является одной из наиболее естественных задач искусственного интеллекта. Но, к сожалению, пока не решена задача обработки текста на естественном языке, так чтобы программа могла отвечать на произвольные вопросы относительно содержащейся в тексте информации. Конечно есть решения данной задачи, к примеру, виртуальный голосовой помощник <<Алиса>> от Яндекса, которая способна распознавать естественную речь и давать ответы на вопросы. Или же <<Cortana>> в ОС Windows 10. Эти программы не точны и не способны полно отвечать на вопросы. Они нацелены на ответы часто задаваемых вопросов.

Теперь о подходах.
Для естественных языков оптимальным вариантом являтся использование DCG нотации вместе с методом расширенных сетей переходов. Для искусственных языков же использование лексического разбора вместе с синтаксическим анализом.

Prolog удобен в решении задач грамматического разбора и анализа естественного языка, ввиду простоты и естественности реализации процессов переборов с возвратами, а также с удобством манипулирования символьной информацией.

## Задание

4.Реализовать синтаксический анализатор арифметического выражения для перевода его в префиксную форму. В выражении допустимы операции +,-,*,/.

## Принцип решения

Я подошел к заданию с двух сторон:
+ Символьные вычисления.
+ Обработка естественного языка.
Для обоих случаев предполагается, что выражение указано правильно.
## Префиксная запись с помощью символьных вычислений.
Для решения поставленной задачи я использовал встроенный предикат ** =.. **, который служит для преобразования структурных термов в список из функтора и аргументов.
Затем рекурсивно использую, описанный мной функтор **ris**, для полученных аргументов. Таким образом строится бинарное дерево. Когда достигли конца(* листа *), у нас в теории есть только один элемент и нет функтора, тогда проверяем является ли этот элемент числом.

```
:-op(700, xfx, ris).
R ris Expr:-
        Expr =.. [Op, A, B],
        A1 ris A, B1 ris B,
        prefix(Op, A1, B1, R),
        printRegex(R), nl.

X ris X:- integer(X).
```
После успешной проверки возвращаемся на уровень выше в факт prefix, осуществляющий подстановку, точнее сказать реверсированием простейшей записи в постфиксную.
```
prefix(Sign, A, B, [Sign, A, B]).
```
## Результаты
Пример вывода:
```
?- X ris -2/5+4*45-89+4.
[/,-2,5]
[*,4,45]
[+,[/,-2,5][*,4,45]]
[-,[+,[/,-2,5],[*,4,45]],89]
[+,[-,[+,[/,-2,5],[*,4,45]],89],4]
X = [+, [-, [+, [/, -2, 5], [*, 4|...]], 89], 4] .

?- X ris -2.
X = -2.

?- X ris +2.
X = 2.
```
## Префиксная запись с помощью обработки естественного языка
Применение DCG нотации заметно упрощает программу и отладку, правда, по началу было непривычно ее использовать.
Точка входа в программу

```
calc(X, ANS):-
    expr(ANS, X, []).
```

Дальше парсим выражение на простые составляющие. Так как самый низкий приоритет имеют операции сложения и вычитания, то они, логично, должны выполняться в последнюю очередь. Разбиваем выражение рекурсивно.
```
expr([+, X, Y]) --> term(X), [+], expr(Y).
expr([-, X, Y]) --> term(X), [-], expr(Y).
expr(Z) --> term(Z).
```
Предикат `term` разбивает выражения на знаки более высокого приоритета(*,/). Если разбивать нечего, то это может быть только числом.
```
term([*, X, Y]) --> number(X), [*], term(Y).
term([/,X,Y]) --> number(X), [/], term(Y).
term(Z) --> number(Z).
```
В предикате `number` рассматриваются частные случаи. Если числа, имеют унарные знаки +/-. Если унарны знаков нет, то получаем простое число.

```
number(C) --> [+], number(C).
number(C) --> [-], number(X),{C is -X}.
number(X) --> [C], {integer(C), X is C}.
```


## Выводы
Данная лабораторная работа очень содержательная, и помогает при написании курсовой работы. Из своего задания я только узнал многое:
+ о встроенном предикате **-..**. А из всей темы очень много.
+ о замечательной, опции, предоставлемой, языком prolog -- DCG нотации.
Я научился работать c разбором контекстно-свободной грамматики с помощью дерева разбора по DCG нотации. Таким образом, можно обрабатывать простой текст автоматически, например, этим занимается автоответчик для Jabber, на основе Prolog.

Это интересный подход, который может сократить бесчисленное количество лишних строчек кода. С помощью данного метода можно реализовывать различные переводчики, которые будут способны учитывать синтаксис, падеж, пунктуационные знаки.

Вот я прикинул, что бы вышло, если я бы писал программу с разбором грамматики. Думаю ничего бы не вышло.




