

sentence(L, R):- sentence(R, L, []).
sentence(sentence(S, V, O)) -->
    subject_clause(S, N), verb_clause(V, N), object_clause(O, _).

subject_clause(subject_clause(S), N) -->
    subject(S, N).

subject_clause(subject_clause(and(S1, S2)), pl) -->
    subject(S1, _), [and], subject(S2, _).

object_clause(object(S), N) -->
    subject(S, N).

subject(subject(C), N) --> noun(C, N).
subject(subject(A, C), N) --> adjective(A), noun(C, N).
subject(subject(A, C), N) --> article(A, N), noun(C, N).

verb_clause(verb_clause(V), N) --> verb(V, N).
verb_clause(verb_clause(not(V)), N) --> negation(N), verb(V, N).

negation(pl) --> [do, not].%pl -- множественное число
negation(s) --> [does, not].%s -- единственное


noun(noun(S, s), s) --> {member(S, [john, mary, student, sport, programming, prolog])}, [S].
noun(noun(S, pl), pl) --> {member(S, [students, sports])}, [S].

adjective(adj(S)) --> {member(S, [smart, stupid, young, old])}, [S].
verb(verb(S, s), s) --> {member(S, [likes, hates, prefers])}, [S].
verb(verb(S, pl), pl) --> {member(S, [like, hate, prefer])}, [S].

article(article(a), s) --> [a].
article(article(the), _) --> [the].

%sentence([mary, does, not, stupid], X).