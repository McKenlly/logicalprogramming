expr([+, X, Y]) --> term(X), [+], expr(Y).
expr([-, X, Y]) --> term(X), [-], expr(Y).
expr(Z) --> term(Z).

term([*, X, Y]) --> number(X), [*], term(Y).
term([/,X,Y]) --> number(X), [/], term(Y).
term(Z) --> number(Z).


number(C) --> [+], number(C).
number(C) --> [-], number(X),{C is -X}.
number(X) --> [C], {integer(C), X is C}.