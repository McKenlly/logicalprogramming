%worked!
member(S, X, D, T).
sentence --> subject_clause, verb_clause, object_clause.
subject_clause --> subject, [and], subject.
subject_clause --> subject.
object_clause --> subject.


subject --> noun.
subject --> adjective, noun.
subject --> article, noun.

article --> [a].
article --> [the].

verb_clause --> verb.
verb_clause --> negation, verb.

negation --> [do, not].
negation --> [does, not].

noun --> member(S, [jonm, mary, student, students, sports, programming, prolog]), [S].
adjective --> member(S, [smart, stupid, young, old]), [S].
verb --> member(S, [likes, like, hates, hate, prefers, prefer]), [S].
