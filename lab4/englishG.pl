%sentence --> noun_phase, verb_phase.

%noun_phase --> determiner, noun.

%verb_phase --> verb, noun_phase.
%verb_phase --> verb.

%determiner --> [the].

%noun --> [apple].
%noun --> [man].

%verb --> [eats].

%verb --> [sings].
%without functor --> DCG
sentence(SO, S):-
    noun_phase(SO, S1).
    verb_phase(S1, S).

noun_phase(SO, S):- determiner(SO, S1), noun(S1, S).

verb_phase(SO, S):- verb(SO, S).
verb_phase(SO, S):- verb(SO,S1), noun(S1, S).

determiner([the|S], S).

noun([man|S], S).
noun([apple|S], S).

verb([eats|S], S).
verb([sings|S], S).