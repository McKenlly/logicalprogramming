
:-op(700, xfx, vis).
vmult([], _, []).
vmult([X|T], T, [X1|T1]):-
    X1 is X*T,
    vmult(T, Y, T1).

vadd([], [], []).
vadd([X|T1], [X1|T2], [Res|T3]):-
    Res is X1+X,
    vadd(T1, T2, T3).

smult([X], [Y], Z):-
    Z is X*Y.
smult([X|T1], [X1|T2], Z):-
    smult(T1, T2, Z1), Z is X*X1+Z1.

comp(*, A, B, R):- vector(A), integer(B), vmult(A, B, R).
comp(*, A, B, R):- vector(B), integer(A), vmult(B, A, R).
comp(*, A, B, R):- vector(A), vector(B), smult(A, B, R).
comp(*, A, B, R):- integer(A), integer(B), R is A*B.

comp(+, A, B, R):- vector(A), vector(B), vadd(A, B, R).
comp(+, A, B, R):- integer(A), integer(B), R is A+B.

vector([]).
vector([_|_]).
R vis Expr:- Expr =.. [Op, A, B], A1 vis A, B1 vis B, comp(Op, A1, B1, R).
X vis X:- vector(X); integer(X).