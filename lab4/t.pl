expr(Y) --> slag(Y).
expr(Y) --> slag(Y1), ['+'], expr(Y2) , {Y is Y1 + Y2}.
expr(Y) --> slag(Y1), ['-'], expr(Y2) , {Y is Y1 - Y2}.
 
slag(Y) --> mnojitel(Y).
slag(Y) --> mnojitel(Y1), ['*'], slag(Y2), {Y is Y1 * Y2}.
slag(Y) --> mnojitel(Y1), ['/'], slag(Y2), {Y is Y1 / Y2}.
mnojitel(Y) --> num(Y).









run(X, Y):-
    expr(X, Y, []).


expr(Y) --> term(Y), addterm(Y).
addterm --> [].
addterm --> [+], expr.
term --> factor, multfactor.
multfactor --> [].
multfactor --> [*], term.
factor --> [I], {integer(I)}.
factor --> ['('], expr, [')'].