ball(black).
ball(white).
ball(empty).


isPos(ball(black), ball(black)).
isPos(ball(empty), ball(black)).
isPos(ball(white), ball(white)).
isPos(ball(white), ball(empty)).
move(Board, NewBoard):-append(Start, [ball(empty), ball(white)|Tail], Board), append(Start, [ball(white), ball(empty)|Tail], NewBoard).
move(Board, NewBoard):-append(Start, [ball(black), ball(empty)|Tail], Board), append(Start, [ball(empty), ball(black)|Tail], NewBoard).
move(Board, NewBoard):-append(Start, [ball(black), ball(white), ball(empty)|Tail], Board), append(Start, [ball(empty), ball(white), ball(black)|Tail], NewBoard).
move(Board, NewBoard):-append(Start, [ball(empty), ball(black), ball(white)|Tail], Board), append(Start, [ball(white), ball(black), ball(empty)|Tail], NewBoard).


prolong([X | T], [Y, X | T]):-
    move(X, Y),
    not(member(Y, [X | Y])).

initBoard(X):- 
    X = [ball(white), ball(white), ball(empty), ball(black), ball(black)].


check([X, Y | Tail]):-
	isPos(X, Y),
	check([Y|Tail]).
check([X, X | []]).

path([X | T], X, [X | T]).
path(P, B, R):- prolong(P, P1), path(P1, B, R).


dfs_search(Board, FinishB, X):-
	path([Board], FinishB, X).

start(X):-
	initBoard(Board),
    permutation(Board, Y),
    check(Y),
	dfs_search(Board, Y, X),
    print(X),
    !.
