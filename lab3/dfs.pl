ball(black).
ball(white).
ball(empty).

moveBlack(ball(black), ball(empty)).
moveBlack(ball(black), _, ball(empty)).
moveWhite(ball(empty), ball(white)).
moveWhite(ball(empty), _, ball(white)).

initBoard(X):- 
    X = [ball(black), ball(black),  ball(black), ball(empty), ball(white), ball(white), ball(white)].



exchange([], []).

exchange([X, Y, Z | Tail], [R, T, F | Other]):-
	(moveBlack(X, Y), R=Y, T=R, F=Z);
    (moveBlack(X, Y, Z), R=Z, T=Y, F=X);
    (moveWhite(X, Y), R=Y, T=R, F=Z);
    (moveWhite(X, Y, Z), R=Z, T=Y, F=X).

% Проверка
exchange([Head | Tail], [Head1 | Other]):-
    exchange(Tail, Other).


sorted(Board, NewBoard):-
	exchange(Board, BoardTmp),
	sort(BoardTmp, NewBoard).
sorted(List, List).

start(NewBoard):-
    initBoard(Board),
    sorted(Board, NewBoard).
