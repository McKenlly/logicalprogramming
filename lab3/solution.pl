ball(black).
ball(white).
ball(empty).


isPos(ball(black), ball(black)).
isPos(ball(empty), ball(black)).
isPos(ball(white), ball(white)).
isPos(ball(white), ball(empty)).
move(Board, NewBoard):-append(Start, [ball(empty), ball(white)|Tail], Board), append(Start, [ball(white), ball(empty)|Tail], NewBoard).
move(Board, NewBoard):-append(Start, [ball(black), ball(empty)|Tail], Board), append(Start, [ball(empty), ball(black)|Tail], NewBoard).
move(Board, NewBoard):-append(Start, [ball(black), ball(white), ball(empty)|Tail], Board), append(Start, [ball(empty), ball(white), ball(black)|Tail], NewBoard).
move(Board, NewBoard):-append(Start, [ball(empty), ball(black), ball(white)|Tail], Board), append(Start, [ball(white), ball(black), ball(empty)|Tail], NewBoard).

initBoard(X):- 
    X = [ball(black), ball(black), ball(black), ball(empty), ball(white), ball(white), ball(white)].


check([X, Y | Tail]):-
	isPos(X, Y),
	check([Y|Tail]).
check([X, X | []]).
%=================PROLONG================
prolong([X | T], [Y, X | T]):-
    move(X, Y),
    not(member(Y, [X | Y])).


%==================DFS===================

pathDfs([X | T], X, [X | T]).
pathDfs(P, B, R):- 
    prolong(P, P1), 
    pathDfs(P1, B, R).


dfs(Board, FinishB, X):-
	pathDfs([Board], FinishB, X).

%==================BFS===================
pathBfs([[X | T] | _], X, [X | T]).
pathBfs([P | Q], B, R):- 
    findall(X, prolong(P, X), L), 
    append(Q, L, QL), 
    !, 
    pathBfs(QL, B, R).

pathBfs([_ | Q], B, R):- 
    pathBfs(Q, B, R).
bfs(A, B, R):- 
    pathBfs([[A]], B, R).

%=================ID======================

id_path([X | T], X, [X | T], _).

%id_path(P, B, R, N):
id_path(Board, FinishB, X, N):- 
    N > 0, 
    prolong(Board, P1), 
    N1 is N - 1, 
    id_path(P1, FinishB, X, N1).

id(Board, FinishB, X):- 
    length(Board, L), 
    N is  L * L, 
    id_path([Board], FinishB, X, N).

%===============PRINT=====================
printBoardPath([]).
printBoardPath([A|T]):-
    printBoardPath(T), 
    nl, print(A).

call_time(G,T) :-
   statistics(runtime,[T0|_]),
   G,
   statistics(runtime,[T1|_]),
   T is T1 - T0.


startBfs(FullPathBfs):-
	initBoard(Board),
    permutation(Board, Y),
    check(Y),
    call_time(bfs(Board, Y, FullPathBfs), Time1),
    print(Time1),
    
    print("BFS:"), nl,
    printBoardPath(FullPathBfs),
    !.
startDfs(FullPathDfs):-
	initBoard(Board),
    permutation(Board, Y),
    check(Y),
    call_time(dfs(Board, Y, FullPathDfs), Time3),
    print(Time3),
    
    print("DFS:"), nl,
    printBoardPath(FullPathDfs),
    !.

startID(FullPathID):-
	initBoard(Board),
    permutation(Board, Y),
    check(Y),
    call_time(id(Board, Y, FullPathID),Time2),
    print(Time2),
    
    print("ID:"), nl,
    printBoardPath(FullPathID),
    !.