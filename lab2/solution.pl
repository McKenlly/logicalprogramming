writebook(astronomy).
writebook(poetry).
writebook(prose).
writebook(piece).


unique([]):-!.
unique([H|T]):-
    member(H, T), !, fail;
    unique(T).

check([]):-!.
check([person(_, Read, Buy, Write)|T]):-
    !, \+ Read = Write, \+ Buy = Write, check(T).

solve(Solve):-
    Solve = [person(alekseev, XRead, XBuy, XWrite), person(borisov, YRead, YBuy, YWrite),
            person(konstantinov, ZRead, ZBuy, ZWrite), person(dmitriev, WRead, WBuy, WWrite)],
    writebook(XWrite), writebook(YWrite),
    writebook(ZWrite), writebook(WWrite),
    unique([XWrite, YWrite, ZWrite, WWrite]),
    
    writebook(XBuy), writebook(YBuy),
    writebook(ZBuy), writebook(WBuy),
    unique([XBuy, YBuy, ZBuy, WBuy]),
    
    writebook(XRead), writebook(YRead),
    writebook(ZRead), writebook(WRead),
    unique([XRead, YRead, ZRead, WRead]),
    
    member(person(_, piece, _, poetry), Solve),
    \+ member(person(_, astronomy, _, prose), Solve),
    \+ member(person(_, _, astronomy, prose), Solve),
    member(person(alekseev, AlekseevRead, AlekseevBuy, _), Solve),
    member(person(borisov, AlekseevBuy, AlekseevRead, _), Solve),
    member(person(dmitriev, _, _, DmitrievWrite), Solve),
    member(person(borisov, DmitrievWrite, _, _), Solve),
    check(Solve).
