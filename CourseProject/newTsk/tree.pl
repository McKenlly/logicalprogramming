child('Бокоч Сергей', 'Бокоч Михаил').
child('Бокоч Сергей', 'Галайко Наталья').
child('Бокоч Юлия', 'Бокоч Михаил').
child('Бокоч Юлия', 'Галайко Наталья').
child('Бокоч Михаил', 'Бокочъ Михаил').
child('Бокоч Михаил', 'Бокоч Агафья').
child('Бокоч Иван', 'Бокочъ Михаил').
child('Бокоч Иван', 'Бокоч Агафья').
child('Шалдыбина Мария', 'Бокочъ Михаил').
child('Шалдыбина Мария', 'Бокоч Агафья').
child('Бокоч Татьяна', 'Бокочъ Михаил').
child('Бокоч Татьяна', 'Бокоч Агафья').
child('Бокоч Любава', 'Бокочъ Михаил').
child('Бокоч Любава', 'Бокоч Агафья').
child('Галайко Наталья', 'Галайко Иван').
child('Галайко Наталья', 'Пивоварова Мария').
child('Лепина Галина', 'Галайко Иван').
child('Лепина Галина', 'Пивоварова Мария').
child('Бокочъ Михаил', 'Бокоч Степан').
child('Бокочъ Михаил', 'Ришко Анна').
child('Бокоч Агафья', 'Апшай Михаил').
child('Бокоч Агафья', 'Поп Анна').
child('Апшай Михаил', 'Апшай Василий').
child('Апшай Михаил', 'Поп Анна').
child('Поп Анна', 'Апшай Василий').
child('Пивоварова Мария', 'Пивоваров Михаил').
child('Пивоварова Мария', 'Поп Анна').
child('Пивоваров Михаил', 'Пивоваров Роман').
child('Пивоваров Михаил', 'Пивоварова Еримия').
child('Галайко Иван', 'Галайко Александр').
child('Галайко Иван', 'Осипова Домнина').
child('Лепин Роман', 'Лепина Галина').
child('Бокоч Данила', 'Бокоч Татьяна').
child('Бокоч Настя', 'Бокоч Любава').

male('Апшай Василий').
male('Апшай Михаил').
male('Бокоч Данила').
male('Бокоч Иван').
male('Бокоч Михаил').
male('Бокоч Настя').
male('Бокоч Сергей').
male('Бокоч Степан').
male('Бокочъ Михаил').
male('Галайко Александр').
male('Галайко Иван').
male('Лепин Роман').
male('Пивоваров Михаил').
male('Пивоваров Роман').
female('Бокоч Агафья').
female('Бокоч Любава').
female('Бокоч Настя').
female('Бокоч Татьяна').
female('Бокоч Юлия').
female('Галайко Наталья').
female('Лепина Галина').
female('Осипова Домнина').
female('Пивоварова Еримия').
female('Пивоварова Мария').
female('Поп Анна').
female('Ришко Анна').
female('Шалдыбина Мария').

find(X, X, 0, 0).

%find(From, To, Dx, Dy):-
%    child(X, To),
%    find(X, To, W, N),
%    Dx is W + 1,
    

%find(From, To, Dx, Dy):-
%    child(To, X),
%    find(X, To, W, N),
%    Dx is W + 1,
move(A,B):-child(A,B);child(B,A). 
move(A,B):-
    append(Begin,[b,'_'|End],A),append(Begin,['_',b|End],B).
move(A,B):-
    append(Begin,['_',w|End],A),append(Begin,[w,'_'|End],B).
move(A,B):-
    append(Begin,[b,w,'_'|End],A),append(Begin,['_',w,b|End],B).
move(A,B):-
    append(Begin,['_',b,w|End],A),append(Begin,[w,b,'_'|End],B).
 
search_dpth(Start,Finish):-dpth([Start],Finish,Way),show_answer(Way).
 
prolong([Temp|Tail],[New,Temp|Tail]):-
    move(Temp,New),not(member(New,[Temp|Tail])).
 
dpth([Finish|Tail],Finish,[Finish|Tail]).
dpth(TempWay,Finish,Way):-
    prolong(TempWay,NewWay),dpth(NewWay,Finish,Way).
 
show_answer([_]):-!.
show_answer([A,B|Tail]):-
    show_answer([B|Tail]),nl,write(B),write(' -> '),write(A).

relative(X, From, To):-
    find(From)

