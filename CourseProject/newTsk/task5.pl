/*          Вопросы, относительно степени родства          */
:- [task3].

question_word--> [how, who, "How", "Who"].

quantity --> [much, many].

purals --> [sisters, brothers, children].
pural(sister, sisters).
pural(brother, brothers).


help_word --> [do, does].

have_has --> [have, has].

is(X):-
    member(X,[is]).

particle(X):-
    member(X, ["s"]).

question_mark --> ['?'].

his_her(X):-
    member(X, [his, her, he, she]).


%how many brothers does *name* have ?
ask_question([IsQuestion, Quantity, WhoRel, IsVerb, Name, IsHave, Sign]):-
    question_word(L, IsQuestion),
    quantity(Tr, Quantity),
    purals(TRR, WhoRel),
    help_word(TRRr,IsVerb),
    (male(Name);female(Name)),
    nb_setval(lastName, Name),
    have_has(DF, IsHave),
    question_mark(TTT, Sign),

    pural(OneWho, WhoRel),
    ((setof(X, closeRelation([Name, X], OneWho), T),
    length(T, A), A>0);
    (setof(X, closeRelationLast([Name, _, X], OneWho), T1),
    length(T1, A1), Ans is A1)),
    write(Name),
    write(" have "),
    ((Ans =:= 1,write(Ans),write(" "),write(OneWho));
    (\+(Ans =:= 1),write(Ans),write(" "),write(WhoRel))),
    !.

%how many brothers does he have ?
ask_question(List):-
    List = [A,B,C1,D,E1,F,H],
    question_word(A),
    quantity(B),
    purals(C1),
    help_word(D),
    his_her(E1),
    nb_getval(lastName,E),
    have_has(F),
    question_mark(H),

    pural(C,C1),
    setof(X,ask_relative(X,E,C),T),
    length(T,Res),
    write(E),
    write(" have "),
    ((Res =:= 1,write(Res),write(" "),write(C));(\+(Res =:= 1),write(Res),write(" "),write(C1))),!.

% who is *name* brother?
ask_question(List):-
    List = [A,B,C,D,E,F],
    question_word(A),
    is(B),
    (male(C);female(C)),
    nb_setval(lastName,C),
    particle(D),
    check_relation(E),
    question_mark(F), !,
    check_link(E,Res,C),
    write(Res),write(" is "), write(C),write("s "),write(E).

% who is her brother
ask_question(List):-
    List = [A,B,C1,D,E],
    question_word(A),
    is(B),
    his_her(C1),
    nb_getval(lastName,C),
    check_relation(D),
    question_mark(E),!,
    check_link(D,Res,C),
    write(Res),write(" is "), write(C),write("s "),write(D).

% is *name* *name* s son?
ask_question(List):-
    List = [A,B,C,D,E,F],
    nb_setval(lastName,C),
    is(A),
    (male(B);female(B)),
    (male(C);female(C)),
    particle(D),
    check_relation(E),
    question_mark(F),
    check_link(E,B,C),
    !.

% is *name* his son?
ask_question(List):-
    List = [A,B,C1,D,E],
    is(A),
    (male(B);female(B)),
    his_her(C1),
    check_relation(D),
    question_mark(E),

    nb_getval(lastName,C),
    check_link(D,B,C),
    !.

/*          Анализ предложений          */

% who is *name* s brother?
% is *name* *name* s son ?
analysis(List,Res):-
    append(L1,L2,List),
    questionType(L1,Q),
    append(M1,M2,L2),
    name(M1,Q1),
    append(N1,N2,M2),
    relationship(N1,Q2),
    questionsign(N2,Q3),
    Res = sentence(Q,Q1,Q2,Q3).

% how many brothers does *name* have ?
analysis(List,Res):-
    List = [A,B,C,D,E,F,G],
    questionType([A,B],Q),
    relationship([C],Q1),
    helpword([D],Q2),
    name([E],Q3),
    have_Has([F],Q4),
    questionsign([G],Q5),
    Res = sentence(Q,Q1,Q2,Q3,Q4,Q5).

relationship([X],Res):-
    check_relation(X),
    Res = relationship_(X).

relationship([X],Res):-
    purals(X),
    Res = relationship_(X).

helpword([X],Res):-
    help_word(X),
    Res = helpWord(X).

have_Has([X],Res):-
    have_has(X),
    Res = haveHas(X).

questionType(L,question_type(X,Y)):-
    question_word(X1),
    is(Y1),
    L = [X1,Y1],
    X = questionWord(X1),
    Y = auxiliaryVerb(Y1).

questionType(L,question_type(X,Y)):-
    question_word(X1),
    quantity(Y1),
    L = [X1,Y1],
    X = questionWord(X1),
    Y = much_many(Y1).

questionType(L,question_type(X)):-
    is(X1),
    L = [X1],
    X = auxiliaryVerb(X1).

name(L,names(X)):-
    L = [X1,Y1],
    ((male(X1);female(X1)); (his_her(X1))),
    particle(Y1),
    X = person(X1,Y1).

name(L,names(X)):-
    L = [X1],
    ((male(X1);female(X1)); (his_her(X1))),
    X = person(X1).

name(L,names(X,Y)):-
    L = [X1,Y1,Z],
    (male(X1);female(X1)),
    ((male(Y1);female(Y1)); (his_her(Y1))),
    particle(Z),
    X = person(X1),
    Y = relative_(Y1,Z).

questionsign([X],Res):-
    question_mark(X),
Res = questionMark(X).