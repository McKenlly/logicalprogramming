
:- [data].

move(From, To) :- 
    child(To, From);
    child(From, To).

prolong([X | T], [Y, X | T]):-
    move(X, Y),
    not(member(Y, [X | Y])).

path([[To | T] | _], To, [To | T]).

path([P | Q], B, R):- 
    findall(X, prolong(P, X), L), 
    append(Q, L, QL), 
    !, 
    path(QL, B, R).

path([_ | Q], B, R):- 
    path(Q, B, R).

bfs(From, To, Path):- 
    path([[From]], To, Path).


%==================================
childr(X, Y, child):-
    child(Y, X).


mother(X, Y, mother):-
    child(X, Y),
    female(Y).


father(X, Y, father):-
    child(X, Y),
    male(Y).


wife(X, Y, wife):-
    child(Z, X),
    child(Z, Y),
    female(Y).


husband(X, Y, husband):-
    child(Z, X),
    child(Z, Y),
    male(Y).


brother(X, Y, brother):-
    child(X, Z),
    child(Y, Z),
    male(Y).


sister(X, Y, sister):-
    child(X, Z),
    child(Y, Z),
    female(Y).
%===============================================================

getFromlist(X, [X|T]).
closeRelation([Curr, Next | []], Who):-
    childr(Curr, Next, Who);
    mother(Curr, Next, Who);
    father(Curr, Next, Who).


closeRelationLast([Curr,_, NNext | []], Who):-
    husband(Curr, NNext, Who);
    wife(   Curr, NNext, Who);
    sister( Curr, NNext, Who);
    brother(Curr, NNext, Who).

distantRelation([_], []).
distantRelation([Curr, Next|Tail], [WhoNext | WhoTail]):-
    (getFromlist(NNext, Tail), closeRelationLast([Curr, Next, NNext], WhoNext), distantRelation(Tail, WhoTail));
    (closeRelation([Curr, Next], WhoNext), distantRelation([Next | Tail], WhoTail)).


allRelation(Path, Who):-
    closeRelationLast(Path, Who); closeRelation(Path, Who);
    distantRelation(Path, Who).

firstList([X|T], X).

lastList([X|[]], X).
lastList([_|T], D):-
    lastList(T, D).

relative(Who, From, To):-
    bfs(To, From, Path),
    firstList(Path, From),
    lastList(Path, To),
    allRelation(Path, Who).
    