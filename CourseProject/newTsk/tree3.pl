child('Бокоч Сергей', 'Бокоч Михаил', 1).
child('Бокоч Сергей', 'Галайко Наталья', 1).
child('Бокоч Юлия', 'Бокоч Михаил', 1).
child('Бокоч Юлия', 'Галайко Наталья', 1).
child('Бокоч Михаил', 'Бокочъ Михаил', 1).
child('Бокоч Михаил', 'Бокоч Агафья', 1).
child('Бокоч Иван', 'Бокочъ Михаил', 1).
child('Бокоч Иван', 'Бокоч Агафья', 1).
child('Шалдыбина Мария', 'Бокочъ Михаил', 1).
child('Шалдыбина Мария', 'Бокоч Агафья', 1).
child('Бокоч Татьяна', 'Бокочъ Михаил', 1).
child('Бокоч Татьяна', 'Бокоч Агафья', 1).
child('Бокоч Любава', 'Бокочъ Михаил', 1).
child('Бокоч Любава', 'Бокоч Агафья', 1).
child('Галайко Наталья', 'Галайко Иван', 1).
child('Галайко Наталья', 'Пивоварова Мария', 1).
child('Лепина Галина', 'Галайко Иван', 1).
child('Лепина Галина', 'Пивоварова Мария', 1).
child('Бокочъ Михаил', 'Бокоч Степан', 1).
child('Бокочъ Михаил', 'Ришко Анна', 1).
child('Бокоч Агафья', 'Апшай Михаил', 1).
child('Бокоч Агафья', 'Поп Анна', 1).
child('Апшай Михаил', 'Апшай Василий', 1).
child('Апшай Михаил', 'Поп Анна', 1).
child('Поп Анна', 'Апшай Василий', 1).
child('Пивоварова Мария', 'Пивоваров Михаил', 1).
child('Пивоварова Мария', 'Поп Анна', 1).
child('Пивоваров Михаил', 'Пивоваров Роман', 1).
child('Пивоваров Михаил', 'Пивоварова Еримия', 1).
child('Галайко Иван', 'Галайко Александр', 1).
child('Галайко Иван', 'Осипова Домнина', 1).
child('Лепин Роман', 'Лепина Галина', 1).
child('Бокоч Данила', 'Бокоч Татьяна', 1).
child('Бокоч Настя', 'Бокоч Любава', 1).

male('Апшай Василий').
male('Апшай Михаил').
male('Бокоч Данила').
male('Бокоч Иван').
male('Бокоч Михаил').
male('Бокоч Настя').
male('Бокоч Сергей').
male('Бокоч Степан').
male('Бокочъ Михаил').
male('Галайко Александр').
male('Галайко Иван').
male('Лепин Роман').
male('Пивоваров Михаил').
male('Пивоваров Роман').
female('Бокоч Агафья').
female('Бокоч Любава').
female('Бокоч Настя').
female('Бокоч Татьяна').
female('Бокоч Юлия').
female('Галайко Наталья').
female('Лепина Галина').
female('Осипова Домнина').
female('Пивоварова Еримия').
female('Пивоварова Мария').
female('Поп Анна').
female('Ришко Анна').
female('Шалдыбина Мария').


revelation(Cur, [Next | RPath]):-
        %format('\nRunBr ~w her ~w\n', [Cur,Next]),
        (brother(Cur,Next, RPath, NNext, RRPath), revelation(NNext, RRPath)).
revelation(Cur, [Next | RPath]):-
        %format('\nRunSist ~w her ~w\n', [Cur,Next]),
        (sister(Cur, Next, RPath, NNext, RRPath), revelation(NNext, RRPath)).
revelation(Cur, [Next | RPath]):-
        %format('\nRunMot ~w her ~w\n', [Cur,Next]),
        (mother(Cur, Next, RPath, NNext, RRPath), revelation(NNext, RRPath)).
        
revelation(Cur, [Next | RPath]):-
        %format('\nRunFath ~w her ~w\n', [Cur,Next]),
        (father(Cur, Next, RPath, NNext, RRPath), revelation(NNext, RRPath)).
    
revelation(Cur, [Next | RPath]):-
        %format('\nRunChild ~w her ~w\n', [Cur,Next]),
        (childr(Cur, Next, RPath, NNext, RRPath), revelation(NNext, RRPath)).
path(From, To, Dist) :- 
    child(To, From, Dist);
    child(From, To, Dist)..


bfs(X,Y,P) :- 
    bfs_b(Y,[n(X,[])],[],R), 
    reverse(R, P). 

bfs_b(Y,[n(Y,P)|_],_,P).

bfs_b(Y,[n(S,P1)|Ns],C,P) :- 
    findall(n(S1,[A|P1]), 
    (path(S,S1,A), not(member(S1,C))), Es), 
    append(Ns,Es,O), 
    bfs_b(Y,O,[S|C],P). 