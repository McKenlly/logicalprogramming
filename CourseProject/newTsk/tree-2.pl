
    
reverse(List, ReverseList):-
    reverse(List, [], ReverseList). % вызов вспомогательной функции с пустым буфером

reverse([], Buffer, Buffer):-!.

reverse([Head|Tail], Buffer, ReverseList):-
    reverse(Tail, [Head|Buffer], ReverseList).

path(From, To) :- 
    child(To, From);
    child(From, To).

shorterPath([H|Path], Dist) :-
    (rpath([H|_], D) -> Dist < D -> retract(rpath([H|_], _)); true),
    assertz(rpath([H|Path], Dist)).

traverse(From, Path, Dist) :-
    path(From, T),
    not(member(T, Path)),
    NewDist is Dist+1,
    shorterPath([T,From|Path], NewDist),
    traverse(T, [From|Path], NewDist).

traverse(From):-
    retractall(rpath(_, _)),
    traverse(From, [], 0).

traverse(_).


childr(_,_,_,_,[]):-!.
childr(X, Y, PPath, Y, PPath):-
    child(Y, X),
    format('-child\n').

mother(_,_,_,_,[]).
mother(X, Y, PPath, Y, PPath):-
    child(X, Y),
    female(Y),
    format('-mother').

father(_,_,_,_,[]).
father(X, Y, PPath, Y, PPath):-
    child(X, Y),
    male(Y),
    format('-father ~w ~w', [X, Y]).

wife(_,_,_,_,[]).
wife(X, Y, [R|PPath], R, PPath):-
    child(Y, X),
    child(Y, R),
    female(R),
    format('-wife').

husband(_,_,_,_,[]).
husband(X, Y, [R|PPath], R, PPath):-
    child(Y, X),
    child(Y, R),
    male(R),
    format('-husband').

brother(_,_,_,_,[]).
brother(X, Y, [R|PPath], R, PPath):-
    child(X, Y),
    child(R, Y),
    male(R),
    format('-brother', [X, Y]).


sister(X, Y, [R|PPath], R, PPath):-
    child(X, Y),
    child(R, Y),
    female(R),
    format('-sister', [X,Y]).

revelation(Cur, [Next | RPath]):-
        format('\n ~w ~w \n', [Cur, Next]),
        childr(Cur, Next, RPath, NNext, RRPath), revelation(NNext, RRPath), !;
        wife(Cur,   Next, RPath, NNext, RRPath), revelation(NNext, RRPath), !;
        husband(Cur,Next, RPath, NNext, RRPath), revelation(NNext, RRPath), !;
        brother(Cur,Next, RPath, NNext, RRPath), revelation(NNext, RRPath), !;
        sister(Cur, Next, RPath, NNext, RRPath), revelation(NNext, RRPath), !;
        mother(Cur, Next, RPath, NNext, RRPath), revelation(NNext, RRPath), !;
        father(Cur, Next, RPath, NNext, RRPath), revelation(NNext, RRPath), !.

rev([Curr|RRPath]):-
    revelation(Curr, RRPath).

inditification(Curr, [Next|Tail]):-
    childr(Cur, Next, RPath, NNext, RRPath);
    mother(Cur, Next, RPath, NNext, RRPath);
    father(Cur, Next, RPath, NNext, RRPath).

calc(From, To, Path):-
    traverse(From),
    rpath([To|RPath], Dist) -> reverse([To|RPath], P), 
    print(P),nl,
    rev(P).