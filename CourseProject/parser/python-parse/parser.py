import os
import sys


if __name__ == "__main__":
	inputFile = open(sys.argv[1])
	isTree = {}
	isID = ""
	isName = ""
	isSurname = ""
	isSex = ""
	currLine = inputFile.readline().split()	
	while len(currLine)!=0:
		if (len(currLine) < 3):
			currLine = inputFile.readline().split()
			continue
		if currLine[2] == 'INDI':
			isID = currLine[1]
			while (currLine[1] != 'GIVN' and currLine[1] != ' _MARNM'):
				currLine = inputFile.readline().split()
			isName = currLine[2]
			while (currLine[1] != 'SURN'):
				currLine = inputFile.readline().split()
			isSurname = currLine[2]
			while (currLine[1] != 'SEX'):
				currLine = inputFile.readline().split()
			isSex = currLine[2]
			isTree[isID] = isSurname + ' ' + isName
			if isSex=='F':
				print("female('{0}').".format(isTree[isID]))
			else:
				print("male('{0}').".format(isTree[isID]))
		elif currLine[2] == 'FAM':
			isWife = ""
			isHusband = ""
			currLine = inputFile.readline().split()
			currLine = inputFile.readline().split()
			if currLine[1] == 'HUSB' and currLine[2] in isTree.keys() :
				isHusband = isTree[currLine[2]]
				currLine = inputFile.readline().split()
			if currLine[1] == 'WIFE' and currLine[2] in isTree.keys():			
				isWife = isTree[currLine[2]]
				currLine = inputFile.readline().split()
			while (currLine[1] == 'CHIL'):
				if (isWife != "" and currLine[2] in isTree.keys()):
					print("child('{0}', '{1}').".format(isTree[currLine[2]], isWife))
				if (isHusband != "" and currLine[2] in isTree.keys()):
					print("child('{0}', '{1}').".format(isTree[currLine[2]], isHusband))
				currLine = inputFile.readline().split()
		currLine = inputFile.readline().split()



