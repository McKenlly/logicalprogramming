:- [task4].

questionStart--> [how, who, whose].

quantity --> [much, few].
union --> [and].
plural --> [sisters, brothers, children].
singular(sister, sisters).
singular(brother, brothers).
singular(child, children).
questionWord --> [do, does].
ruleQuestion --> [have, has].
isVerbQuestion --> [is, are].
isExist(X):-
    male(X);
    female(X).
possessivePronouns --> [his, her, he, she, him, it].

writeAnsForQuestions(Name, IsVerb, Who, WhoRel, L):-
    (L =:= 0, format("Sorry, ~w ~w not ~w.\n", [Name, IsVerb, WhoRel]), !);
    (L =:= 1, format("~w ~w ~w ~w.\n", [Name, IsVerb, L, Who]), !);
    (format("~w ~w ~w ~w.\n", [Name, IsVerb, L, WhoRel]), !).


writeAnsForQuestions(Name, is, WhoRel, Ans):-
    format("~w is ~w of ~w.\n", [Ans, WhoRel, Name]).



question([IsQuestion, Quantity, WhoRel, IsVerb, Name, IsHave]):-
    questionStart(_, IsQuestion),
    quantity(_, Quantity),
    plural(_, WhoRel),
    questionWord(_,IsVerb),
    isExist(Name),
    nb_setval(lastName, Name),
    ruleQuestion(_, IsHave),
    singular(OneWho, WhoRel),
    (
        (
        bagof(X, closeRelation([Name, X], OneWho), T),
        length(T, A), 
        A > 0
        );
        (
        bagof(X, closeRelationLast([Name, _, X], OneWho), T),
        length(T, A) 
        )
    ),
    Ans is A,
    writeAnsForQuestions(Name, has, OneWho, WhoRel, Ans),
    !.

question([IsQuestion, Quantity, WhoRel, IsVerb, Unknown, IsHas]):-
    questionStart(_, IsQuestion),
    quantity(_, Quantity),
    plural(_, WhoRel),
    questionWord(_,IsVerb), 
    possessivePronouns(_, Unknown),
    nb_getval(lastName, Name),
    ruleQuestion(_, IsHas),
    singular(OneWho, WhoRel),
    (
        (
        setof(X, closeRelation([Name, X], OneWho), T),
        length(T, A), 
        A > 0
        );
        (
        setof(X, closeRelationLast([Name, _, X], OneWho), T),
        length(T, A) 
        )
    ),
    Ans is A,
    writeAnsForQuestions(Name, IsHas, OneWho, WhoRel, Ans),
    !.

question([IsQuestion, IsVerb, Name, WhoRel]):-
    questionStart(_, IsQuestion),
    plural(_, WhoRel),
    isVerbQuestion(_, IsVerb),
    isExist(Name),
    nb_setval(lastName, Name),
    (
        closeRelation([Name, X], WhoRel);
        closeRelationLast([Name, _, X], WhoRel)
    ),
    writeAnsForQuestions(Name, IsVerb, WhoRel, X).

question([IsQuestion, IsVerb, Unknown, WhoRel]):-
    questionStart(_, IsQuestion),
    plural(_, WhoRel),
    possessivePronouns(_, Unknown),
    isVerbQuestion(_, IsVerb),
    nb_getval(lastName, Name),
    (
        closeRelation([Name, X], WhoRel);
        closeRelationLast([Name, _, X], WhoRel)
    ),
    writeAnsForQuestions(Name, IsVerb, WhoRel, X).


question([IsQuestion, IsVerb, Name, WhoRel]):-
    questionStart(_, IsQuestion),
    isVerbQuestion(_, IsVerb),
    isExist(Name),
    nb_setval(lastName, Name),
    plural(_, WhoRel),
    (
        closeRelation([X, Name], WhoRel);
        closeRelationLast([X, _, Name], WhoRel)
    ),
    writeAnsForQuestions(Name, IsVerb, WhoRel, X).

question([IsQuestion, IsVerb, Unknown, WhoRel]):-
    questionStart(_, IsQuestion),
    isVerbQuestion(_, IsVerb),
    possessivePronouns(_, Unknown),
    plural(_, WhoRel),
    nb_getval(lastName, Name),
    (
        closeRelation([X, Name], WhoRel);
        closeRelationLast([X, _, Name], WhoRel)
    ),
    writeAnsForQuestions(Name, IsVerb, WhoRel, X).

question([IsQuestion, Name1, IsUnion, Name2, WhoRel]):-
    isVerbQuestion(_, IsQuestion),
    union(_, IsUnion),
    isExist(Name1),
    isExist(Name2),
    ((male(Name1), male(Name2));
    (female(Name1), female(Name2))),
    singular(WhoOne, WhoRel),
    nb_setval(lastName, Name2),
    ((
        closeRelation([Name1, Name2], WhoRel);
        closeRelationLast([Name1, _, Name2], WhoOne)
    ),
    format("YES\n"), !);
    format("NO\n").
