:- [task5_1].

sentence(L, R):- question_sentence(R, L, []); simple_sentence(R, L, []).

simple_sentence(simple_sentence(S, V, O)) -->
    subject_clause(S, N), verb_clause(V, N), object_clause(O, _), [.].

% Do you like your parents
question_sentence(question_sentence(D, S, V, O, K)) -->
    question_clause(D, _), subject_clause(S, N), verb_clause(V, N), object_clause(O, H),object_clause(K, H), [?].

% 'Is',your, sisters, likes, you, ?
question_sentence(question_sentence(D, K, A, S, V)) -->
    question_clause(D, _), object_clause(K, _), object_clause(A, _), verb_clause(S, _), subject_clause(V, _), [?].

question_clause(question_clause(S), N) -->
    question_obj(S, N).

question_obj(question_obj(S), _) --> {member(S,[who, 'Who', 'Where', 'Does', 'Why',whose, 'Whose', 'What', what, when, 'When', do, 'Do'])}, [S].

question_obj(question_obj(A, B), _) --> noun(A, B).
question_obj(question_obj(A, B), _) --> verb(A, B).
subject_clause(subject_clause(S), N) -->
    subject(S, N).

subject_clause(subject_clause(and(S1, S2)), pl) -->
    subject(S1, _), [and], subject(S2, _).

object_clause(object(S), N) -->
    subject(S, N).

subject(subject(C), N) --> noun(C, N).
subject(subject(A, C), N) --> adjective(A), noun(C, N).
subject(subject(A, C), N) --> article(A, N), noun(C, N).

verb_clause(verb_clause(V), N) --> verb(V, N).
verb_clause(verb_clause(not(V)), N) --> negation(N), verb(V, N).

negation(pl) --> [do, not].%pl -- множественное число
negation(s) --> [does, not].%s -- единственное


noun(noun(S, s), s) --> {member(S, ['He', my, he, you, 'She', she,  your, 'I', i, me, him, his, her, family, sister, brother, mother, father, child, friend])}, [S].

noun(noun(S, s), s) --> {member(S, ['Апшай Василий', 'Апшай Михаил', 'Бокоч Данила', 'Бокоч Иван', 'Бокоч Михаил',  'Бокоч Настя', 'Бокоч Сергей', 
'Бокоч Степан','Бокочъ Михаил', 'Галайко Александр', 'Галайко Иван', 'Лепин Роман', 'Пивоваров Михаил',
'Пивоваров Роман','Бокоч Агафья', 'Бокоч Любава', 'Бокоч Настя', 'Бокоч Татьяна',
'Бокоч Юлия', 'Галайко Наталья', 'Лепина Галина', 'Осипова Домнина', 'Пивоварова Еримия', 'Пивоварова Мария',
'Поп Анна','Ришко Анна', 'Шалдыбина Мария'])}, [S].

noun(noun(S, pl), pl) --> {member(S, ['You', you, we, 'We', 'They', they,your, us, sisters, brothers, fathers, mothers, parents, grandmothers])}, [S].

adjective(adj(S)) --> {member(S, [make, like])}, [S].
verb(verb(S, s), s) --> {member(S, [likes, hates, is,'Is', 'It', it, prefers, has, 'Has',have, 'Have'])}, [S].
verb(verb(S, pl), pl) --> {member(S, [like, hate, have, 'Have'])}, [S].

article(article(a), s) --> [a].
article(article(the), _) --> [the].
