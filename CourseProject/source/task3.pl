:- [data].

brother(X, Y):-
    child(Y, Z),
    child(X, Z),
    not(X==Y).

brotherFindAll(X, Y):-
    child(X, Z),
    brother(Z, F),
    child(Y, F),
    male(Y).
