student(102,'Петров').
student(101,'Петровский').
student(104,'Иванов').
student(102,'Ивановский').
student(104,'Запорожцев').
student(101,'Сидоров').
student(103,'Сидоркин').
student(102,'Биткоинов').
student(103,'Эфиркина').
student(103,'Сиплюсплюсов').
student(103,'Программиро').
student(104,'Джаво').
student(103,'Клавиатурникова').
student(101,'Мышин').
student(104,'Фулл').
student(101,'Безумников').
student(102,'Шарпин').
student(104,'Круглосчиталкин').
student(103,'Решетников').
student(102,'Эксель').
student(102,'Текстописов').
student(103,'Текстописова').
student(101,'Густобуквенникова').
student(102,'Криптовалютников').
student(104,'Блокчейнис').
student(102,'Азурин').
student(103,'Вебсервисов').
student(102,'Круглотличников').
subject('LP','Логическое программирование').
subject('MTH','Математический анализ').
subject('FP','Функциональное программирование').
subject('INF','Информатика').
subject('ENG','Английский язык').
subject('PSY','Психология').
grade('Петров','LP',4).
grade('Петров','MTH',3).
grade('Петров','FP',5).
grade('Петров','INF',4).
grade('Петров','ENG',4).
grade('Петров','PSY',3).
grade('Петровский','LP',5).
grade('Петровский','MTH',5).
grade('Петровский','FP',5).
grade('Петровский','INF',4).
grade('Петровский','ENG',5).
grade('Петровский','PSY',3).
grade('Иванов','LP',5).
grade('Иванов','MTH',3).
grade('Иванов','FP',4).
grade('Иванов','INF',5).
grade('Иванов','ENG',5).
grade('Иванов','PSY',5).
grade('Ивановский','LP',3).
grade('Ивановский','MTH',5).
grade('Ивановский','FP',4).
grade('Ивановский','INF',3).
grade('Ивановский','ENG',5).
grade('Ивановский','PSY',5).
grade('Запорожцев','LP',2).
grade('Запорожцев','MTH',2).
grade('Запорожцев','FP',4).
grade('Запорожцев','INF',3).
grade('Запорожцев','ENG',4).
grade('Запорожцев','PSY',4).
grade('Сидоров','LP',4).
grade('Сидоров','MTH',4).
grade('Сидоров','FP',4).
grade('Сидоров','INF',4).
grade('Сидоров','ENG',3).
grade('Сидоров','PSY',4).
grade('Сидоркин','LP',4).
grade('Сидоркин','MTH',5).
grade('Сидоркин','FP',4).
grade('Сидоркин','INF',3).
grade('Сидоркин','ENG',4).
grade('Сидоркин','PSY',4).
grade('Биткоинов','LP',5).
grade('Биткоинов','MTH',4).
grade('Биткоинов','FP',3).
grade('Биткоинов','INF',4).
grade('Биткоинов','ENG',3).
grade('Биткоинов','PSY',2).
grade('Эфиркина','LP',2).
grade('Эфиркина','MTH',4).
grade('Эфиркина','FP',3).
grade('Эфиркина','INF',2).
grade('Эфиркина','ENG',2).
grade('Эфиркина','PSY',4).
grade('Сиплюсплюсов','LP',4).
grade('Сиплюсплюсов','MTH',4).
grade('Сиплюсплюсов','FP',4).
grade('Сиплюсплюсов','INF',5).
grade('Сиплюсплюсов','ENG',4).
grade('Сиплюсплюсов','PSY',4).
grade('Программиро','LP',5).
grade('Программиро','MTH',4).
grade('Программиро','FP',3).
grade('Программиро','INF',3).
grade('Программиро','ENG',5).
grade('Программиро','PSY',4).
grade('Джаво','LP',4).
grade('Джаво','MTH',4).
grade('Джаво','FP',3).
grade('Джаво','INF',2).
grade('Джаво','ENG',3).
grade('Джаво','PSY',5).
grade('Клавиатурникова','LP',4).
grade('Клавиатурникова','MTH',5).
grade('Клавиатурникова','FP',3).
grade('Клавиатурникова','INF',5).
grade('Клавиатурникова','ENG',4).
grade('Клавиатурникова','PSY',4).
grade('Мышин','LP',5).
grade('Мышин','MTH',4).
grade('Мышин','FP',5).
grade('Мышин','INF',4).
grade('Мышин','ENG',3).
grade('Мышин','PSY',3).
grade('Фулл','LP',4).
grade('Фулл','MTH',4).
grade('Фулл','FP',4).
grade('Фулл','INF',5).
grade('Фулл','ENG',5).
grade('Фулл','PSY',5).
grade('Безумников','LP',3).
grade('Безумников','MTH',5).
grade('Безумников','FP',3).
grade('Безумников','INF',2).
grade('Безумников','ENG',4).
grade('Безумников','PSY',3).
grade('Шарпин','LP',3).
grade('Шарпин','MTH',4).
grade('Шарпин','FP',4).
grade('Шарпин','INF',5).
grade('Шарпин','ENG',5).
grade('Шарпин','PSY',5).
grade('Круглосчиталкин','LP',5).
grade('Круглосчиталкин','MTH',2).
grade('Круглосчиталкин','FP',4).
grade('Круглосчиталкин','INF',3).
grade('Круглосчиталкин','ENG',4).
grade('Круглосчиталкин','PSY',3).
grade('Решетников','LP',4).
grade('Решетников','MTH',4).
grade('Решетников','FP',5).
grade('Решетников','INF',3).
grade('Решетников','ENG',5).
grade('Решетников','PSY',3).
grade('Эксель','LP',4).
grade('Эксель','MTH',4).
grade('Эксель','FP',4).
grade('Эксель','INF',3).
grade('Эксель','ENG',5).
grade('Эксель','PSY',4).
grade('Текстописов','LP',2).
grade('Текстописов','MTH',5).
grade('Текстописов','FP',4).
grade('Текстописов','INF',3).
grade('Текстописов','ENG',5).
grade('Текстописов','PSY',3).
grade('Текстописова','LP',4).
grade('Текстописова','MTH',4).
grade('Текстописова','FP',4).
grade('Текстописова','INF',3).
grade('Текстописова','ENG',5).
grade('Текстописова','PSY',2).
grade('Густобуквенникова','LP',5).
grade('Густобуквенникова','MTH',2).
grade('Густобуквенникова','FP',4).
grade('Густобуквенникова','INF',5).
grade('Густобуквенникова','ENG',4).
grade('Густобуквенникова','PSY',3).
grade('Криптовалютников','LP',5).
grade('Криптовалютников','MTH',2).
grade('Криптовалютников','FP',2).
grade('Криптовалютников','INF',3).
grade('Криптовалютников','ENG',4).
grade('Криптовалютников','PSY',2).
grade('Блокчейнис','LP',5).
grade('Блокчейнис','MTH',2).
grade('Блокчейнис','FP',5).
grade('Блокчейнис','INF',3).
grade('Блокчейнис','ENG',4).
grade('Блокчейнис','PSY',5).
grade('Азурин','LP',4).
grade('Азурин','MTH',2).
grade('Азурин','FP',4).
grade('Азурин','INF',3).
grade('Азурин','ENG',5).
grade('Азурин','PSY',2).
grade('Вебсервисов','LP',3).
grade('Вебсервисов','MTH',4).
grade('Вебсервисов','FP',3).
grade('Вебсервисов','INF',3).
grade('Вебсервисов','ENG',5).
grade('Вебсервисов','PSY',2).
grade('Круглотличников','LP',4).
grade('Круглотличников','MTH',3).
grade('Круглотличников','FP',5).
grade('Круглотличников','INF',5).
grade('Круглотличников','ENG',3).
grade('Круглотличников','PSY',5).


%Задание 1==========================================
%Получить таблицу групп и средний балл по каждой из групп
length([], 0).
length([X|Y], L):-
    length(Y, L1),
    L is L1 + 1.

sumMark([], 0).
sumMark([X|Y], Result):-
    sumMark(Y, X1),
    Result is X + X1.
    
sumAllMark([], 0, 0).
sumAllMark([Name|Tail], Answer, Counts):-
    %оценки name студента в ListMark
    sumAllMark(Tail, Answer1, Counts1),
    findall(Mark, grade(Name, _, Mark), ListMark),
    sumMark(ListMark, Result),
    length(ListMark, Counts2),
    Answer is (Result + Answer1),
    Counts is Counts1 + Counts2.

averageGroup([]).
averageGroup([Group|OtherGroup]):-
    averageGroup(OtherGroup),
    findall(Name, student(Group, Name), ListName),
    sumAllMark(ListName, AllMark, Counts),
    Mark is AllMark/Counts,
    write(Group), write(' '), write(Mark), nl.

%Удаляет все вхождения H  в T.
delete_all(_, [], []).
delete_all(X, [X|T], R):-
    delete_all(X, T, R).
delete_all(X, [Y|T], [Y|R]):-
    delete_all(X, T, R).


list_set([], []).
list_set([H|T], [H|T1]):- 
        delete_all(H, T, T2), 
        list_set(T2, T1). 
                
printAverageGroups(_):-
    findall(Group, student(Group, _), OldList),
    list_set(OldList, ListGroup),
    averageGroup(ListGroup).

%Задание 2===========================================
%Для каждого предмета получить список студентов, не сдавших экзамен (grade=2)

findGrade2([]).
findGrade2([X|T]):-
    findall(Name, grade(Name, X, 2), StudentBadBad),
    write(X), write(' '), write(StudentBadBad), nl,
    findGrade2(T).

badStudents(_):-
    findall(Subject, grade(Name, Subject, Mark), ListSub),
    list_set(ListSub, SetSub),
    findGrade2(SetSub). 

%Задание 3==========================================
%Найти количество не сдавших студентов в каждой из групп
intersection([],_,[]). 
intersection([H|T1],S2,[H|T]):- 
              member(H,S2), 
              !,
              intersection(T1,S2,T). 
intersection([_|T],S2,S):-
              intersection(T,S2,S). 

findBadColleagues([]).
findBadColleagues([X|Tail]):-
    findBadColleagues(Tail),
    findall(People, student(X, People), AllStudGroup), 
    findall(Name, grade(Name, _, 2), BadStudents),
    %Случай, когда несколько двоек по экзаменам\
    intersection(AllStudGroup, BadStudents, BadStudGroup),
    length(BadStudGroup, ANS),
    write(X), write(' '), write(ANS), nl.
    

badColleagues(_):-
    findall(Group, student(Group, _), OldList),
    list_set(OldList, ListGroup),
    findBadColleagues(ListGroup).
